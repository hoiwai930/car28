(ns car28.core
  (:require [clj-http.client :as client]
            [net.cgrand.enlive-html :as h]
            [chime :refer [chime-at]]
	    [matchbox.core :as m]
            [clj-time.core :as tt]
            [clj-time.periodic :refer [periodic-seq]]
            [clojure.tools.logging :as log])
  (:use     [clojure.pprint ])
  (:gen-class))

(def query
  {
   :h_f_pr 3 ;20-50k
   :h_f_eg 2 ;1500cc
   :h_f_tr 1 ;AT
   :h_f_se 5 ;seats
   :h_f_ty 1 ;private car
   :h_f_yr 10 ;years
   :h_f_do 1 ;not sell
   :h_page 1
   })

(defn search-contact
  [n]
  (let [r (client/post "https://28car.com/sell_lst.php"
                       {:form-params {
                                      :h_srh_ty 4 ;contact
                                      ;:h_srh 61279047
                                      :h_srh n
                                      }
                        })]

    (spit "/tmp/srh.htm" (:body r) :encoding "Big5" )
    (h/html-snippet (:body r))
    ))

(defn no_cars_per_contact
  [n]
  (count (h/select n [[:td (h/attr-starts :id "rw_")]])))

(defn car-url
  [n]
  (str "https://28car.com/sell_dsp.php?h_vid=" n))

(defn get-id-2
  [n]
  (re-find #"\d{9}"
           (str
             (h/attr-values
               (first 
                 (h/select n [:tr (h/attr? :onclick) ])
                 )
               :onclick))))

(defn img-tags
  [n]
  (future (Thread/sleep 500)
          (-> n
              car-url
              client/get
              :body
              h/html-snippet
              (h/select [[:img (h/attr? :onerror)]]))))

(defn img-urls
  [n]
  (let [
        tags (img-tags n)
        imgs (map #(-> % :attrs :src) @tags)
        jpg (map #(re-find #"http.*jpg" %) imgs)]
    (remove nil? jpg)))

(defn labelling
  [n]
    (-> (apply assoc {}
           (interleave [:brand :name :capacity :cc :transmission :year :price :replies :views :desp :contact :date :time]
                       (filter #(not= (first n) %) n)))))

(defn single-hit-only-2
  [n]
  (let [a (future (Thread/sleep 500)
                (= 1 (-> n
                         :contact
                         (clojure.string/split #":" )
                         last
                         search-contact
                         no_cars_per_contact)))]
        @a))

(defn fetch
  []
  (let [r (client/post "https://28car.com/sell_lst.php"
                       {:form-params query })
        b (h/html-snippet (:body r))
        rows (h/select b [[:td (h/attr-starts :id "rw_")]])
        fields (map #(h/select % [:tr :tr :td h/text-node]) rows)
        ;parse car fields
        fields2 (map labelling fields)
        ;add id
        ids (map get-id-2 rows)
        ;assoc id
        fields3 (map #(assoc %1 :id %2) fields2 ids)
        ;filter out private sales
        cars (filter single-hit-only-2 fields3)
        pics (map #(img-urls (:id %)) cars)
        cars2 (map #(assoc %1 :pics %2) cars pics)
        ]
    cars2
    ;extract pics
    ))

(defn push
  []
  (let [r (m/connect "https://car-90e8c.firebaseio.com/cars")]
    (doseq [n (fetch)]
      (log/info "atomic")
      (m/merge! r {(keyword (:id n)) n}))))

(def every-hour
  (->> (periodic-seq (.. (tt/now)
                         (withTime 00 01 0 0))
                     (-> 5 tt/minutes))))

#_(def cancel-sch!
  (chime-at every-hour
            (fn [time]
              (log/info "push @" time)
              (push))))

(comment
  (boot.core/merge-env! :dependencies '[[jarohen/chime "0.1.9"]])
  (def cancel-sch!
    (chime-at every-hour
              (fn [time]
                (log/info "push @" time)
                (push))))
  )
